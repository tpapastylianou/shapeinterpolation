**ShapeInterpolation** is a matlab package for perfoming shape-based interpolation
on binary and fuzzy / grayscale objects.

Example use:

       >> import ShapeInterpolation.*
       >> Out = gShapeInterp (My3DVolume, 30, 'verbose');
or simply

       >> Out = ShapeInterpolation.gShapeInterp (My3DVolume, 30, 'verbose');
  
Type `help ShapeInterpolation` at the matlab prompt for more details about the package contents.

(c) 2016 - Tasos Papastylianou
