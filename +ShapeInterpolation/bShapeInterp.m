function Out = bShapeInterp(In,Z)
% BSHAPEINTERP - perform Shape-Based Interpolation on binary objects
%    BSHAPEINTERP(In, Z) takes a 3D binary object and performs shape-based
%    interpolation along the z-axis. The intended use of this function is
%    to take segmentations that tend to be of good resolution in the x/y
%    axes, but poor resolution in the z-axis (i.e. between slices), and
%    perform smooth shape-based interpolation along the z-axis.
%
%    This function depends on the "bwdist" function from the Image
%    Processing Toolbox. Note that the mask needs to be defined at all
%    levels otherwise the bwdist function returns NaN at that level, which
%    messes up the interpolation.
%   
%    The algorithm is a simplified version of the method described in:
%     Herman GT, Zheng J, Bucholtz CA. "Shape-based Interpolation". 
%     IEEE Computer Graphics and Applications, 1992, 12, 69-79
%
%    Example:
%
%     % Import the bShapeInterp function from the ShapeInterpolation package
%     import ShapeInterpolation.bShapeInterp;
%
%     % Create a 'rhomboid' shape of size 100x100x9
%      V = zeros(100,100,10);
%      V(40:60,40:60,1) = 1; V(35:65,35:65,2) = 1; V(30:70,30:70,3) = 1;
%      V(25:75,25:75,4) = 1; V(20:80,20:80,5) = 1; V(25:75,25:75,6) = 1;
%      V(30:70,30:70,7) = 1; V(35:65,35:65,8) = 1; V(40:60,40:60,9) = 1;
%
%      % Display the uninterpolated 3D object using an isosurface
%      figure; isosurface(V,0.5);
% 
%      % Perform shape-based interpolation to a size of 100x100x90
%      DV = bShapeInterp(V,90); 
%      figure; isosurface(DV,0.5);
%
%    See also: scatteredInterpolant, bwdist, gShapeInterp

% Copyright 2015 Tasos Papastylianou
% v2.0.0

  import ShapeInterpolation.*

  assert (ndims (In) == 3,  'Param ''In'' needs to be a 3D array');
  assert (Z > size (In, 3), 'Param ''Z'' needs to exceed the number of z slices in ''In''');
  assert (isBinary (In),    '''In'' should be a binary mask of ones and zeros');
  
  [Xg, Yg, Zg] = ndgrid (1 : size (In, 1), 1 : size (In, 2), 1 : size (In, 3));
  [x, y, z]    = ndgrid (1 : size (In, 1), 1 : size (In, 2), linspace (1, size (In, 3), Z));
  
  Pos_dist = zeros (size (In));
  Neg_dist = zeros (size (In));
  Iterations = size (In, 3);
  
  for i = 1 : Iterations
    Pos             = bwdist (In(:,:,i));
    Pos(Pos>0)      = Pos(Pos>0) - 0.5;
    Pos_dist(:,:,i) = Pos;
    Neg             = -bwdist (1-In(:,:,i));
    Neg(Neg<0)      = Neg(Neg<0) + 0.5;
    Neg_dist(:,:,i) = Neg;
  end
  
  D   = double (Pos_dist + Neg_dist);
  F   = scatteredInterpolant (Xg(:), Yg(:), Zg(:), D(:));
  Out = F (x,y,z);
  Out = double (Out <= 0);  
end

