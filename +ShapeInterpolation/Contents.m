% The SHAPEINTERPOLATION package contains functions for performing
% shape-based interpolation on binary and fuzzy / grayscale objects.
% 
% Functions provided: 
%    bShapeInterp - perform Shape-Based Interpolation on binary objects
%    gShapeInterp - perform Shape-based Interpolation on fuzzy/grayscale objects
%
% Helper functions included in package for internal use:
%    isBinary     - check if a variable is binary
%    isFuzzy      - check if a variable is fuzzy
%    sec2TimeStr  - convert seconds to human readable time string
