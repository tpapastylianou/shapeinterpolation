function Out = gShapeInterp (In, Z, varargin)
% GSHAPEINTERP - Perform Shape-based Interpolation on fuzzy/grayscale objects
%     Out = GSHAPEINTERP (In, Z) takes a grayscale object 'In' and performs
%     shape-based interpolation along the Z-axis, resulting in 'Z' slices. 
% 
%     The intended use of this function is to take fuzzy segmentation outputs
%     (i.e. masks with values in the range [0,1]), that tend to be of good
%     resolution in the x/y axes, but poor resolution in the z-axis (i.e.
%     between slices), and perform smooth shape-based interpolation along the
%     z-axis such that it results in a fuzzy object of better z-axis
%     resolution. However, in theory the shape interpolation should work well
%     for normal grayscale images too (i.e. not representing segmentation
%     objects), as long as shape interpolation with respect to the structures
%     and intensities involved is meaningful.
% 
%     Out = GRAYSHAPEINTERP (In, Z, 'parallel') takes advantage of matlab's
%     parallelisation facilities
% 
%     Out = GRAYSHAPEINTERP (In, Z, 'verbose') displays information on the
%     terminal on how many iterations have completed so far and estimated time
%     of completion. Note that 'verbose' does not work together with
%     'parallel', so if you specify both, 'verbose' will be ignored.
% 
%     The algorithm relies on a simplified version of the binary method
%     described in: 
%       Herman GT, Zheng J, Bucholtz CA. "Shape-based Interpolation". 
%       IEEE Computer Graphics and Applications, 1992, 12, 69-79
%     but performed at all intensity levels of the input image by thresholding.
%     The end result is the union of all the resulting shapes, weighed by the
%     intensity at which they were thresholded.
% 
%     See also: bShapeInterp

% (c) 2016 - Tasos Papastylianou
% v1.1.0

import ShapeInterpolation.*

%% Input argument sanity checks and options handling
  Parallelised = false;
  Verbose      = false;
  for i = 1 : length(varargin)
    if strcmp(varargin{i},'parallel') || strcmp(varargin{i},'Parallel')
       Parallelised = true;
       if Verbose == true
         warning('Cannot be verbose when process is parallelised. Turning Verbose off.');
         Verbose = false;
       end
    end
    if strcmp(varargin{i},'verbose') || strcmp(varargin{i},'Verbose')
       Verbose = true;
       if Parallelised == true
         warning('Cannot be verbose when process is parallelised. Turning Verbose off.');
         Verbose = false;
       end
    end
  end

  assert (ndims (In) == 3, 'The Input object needs to be a 3D array');
  assert (Z > size (In, 3), ...
    'The number of output slices needs to exceed the number of slices in the Input object');
  assert (isFuzzy (In), 'Input object needs to be fuzzy');

%% Main body of algorithm
  Out        = zeros (size (In, 1), size (In, 2), Z);
  Levels     = unique(In)';
  Iterations = length(Levels);
  LoopStart  = tic;  

  if Parallelised
    parfor n = 1 : Iterations
      Out = max(Out, Levels(n) .* bShapeInterp (In >= Levels(n), Z));
    end    
    return
  end

  % non parallelised version
  for n = 1 : Iterations
    Out = max(Out, Levels(n) .* bShapeInterp (In >= Levels(n), Z));
    if Verbose
      TimeLeft = toc (LoopStart) * (Iterations - n) / n;
      fprintf('%% gShapeInterp: Iteration %d of %d -- Estimated Time Left: %s\n', n, Iterations, sec2TimeStr (TimeLeft));
    end
  end  
end
