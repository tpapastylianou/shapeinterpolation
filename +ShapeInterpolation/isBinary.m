function Bool = isBinary (In)
% ISBINARY - check if a variable is binary
%    ISBINARY(In) checks if the input 'In' is a binary variable, i.e. only 
%    contains values in the set {0,1}.
%
%    See also: isFuzzy

% (c) 2016 - Tasos Papastylianou
% v1.0.0

  Bool = In == logical(In);   % if binary, this will result in a 'ones' matrix.
  Bool = all(Bool(:));        % return true for 'ones', otherwise false.
end

