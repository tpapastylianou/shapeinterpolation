function Out = sec2TimeStr (In)
% SEC2TIMESTR - convert seconds to human readable time string
%    SEC2TIMESTR(In) takes an 'In' argument expressing the number of seconds, 
%    such as the output of the tic/toc functions, and returns a string that 
%    translates that into more human readable time, such as minutes, days, etc

% (c) 2016 - Tasos Papastylianou
% v1.0.0
  
  Seconds = mod (In, 60);  In = floor (In / 60);
  Minutes = mod (In, 60);  In = floor (In / 60);
  Hours   = mod (In, 24);  In = floor (In / 24);
  Days    = mod (In, 365); In = floor (In / 365);
  Years   = In;

  if Years   == 0; Y = ''; else Y = sprintf('%d years, ', Years);     end
  if Days    == 0; D = ''; else D = sprintf('%d days, ',  Days);      end
  if Hours   == 0; H = ''; else H = sprintf('%d hours, ', Hours);     end
  if Minutes == 0; M = ''; else M = sprintf('%d minutes and ', Minutes); end
  if Seconds == 0; S = ''; else S = sprintf('%d seconds', floor(Seconds)); end
  
  Out = sprintf('%s%s%s%s%s', Y, D, H, M, S);  
end
