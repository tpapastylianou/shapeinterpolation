function Bool = isFuzzy (In)
% ISFUZZY - check if a variable is fuzzy
%    ISFUZZY(In) checks if the input 'In' is a binary variable, i.e. only 
%    contains values in the range [0,1].
%
%    See also: isBinary

% (c) 2016 - Tasos Papastylianou
% v1.0.0

  Bool = false;
  if totalmin(In) < 0; return; end
  if totalmax(In) > 1; return; end
  Bool = true; 
end

function Out = totalmin(In)
  Out = min(In(:));
end

function Out = totalmax(In)
  Out = max(In(:));
end

